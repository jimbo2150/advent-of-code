# Advent Of Code

This repository holds my [Advent of Code](https://adventofcode.com) solutions in PHP.

This repository uses pure PHP with no external libraries or frameworks.

Instead of creating a new file for each puzzle part/segment, I adapt the existing
file to work with both parts where possible.