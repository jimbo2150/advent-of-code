<?php

$start = microtime(true);

$inputFilename = './input.txt';
$inputFile = fopen($inputFilename, 'r');

$data = (function() use ($inputFile) {

	function matchNumbers(string $input): array {
		preg_match_all('/\d+/', $input, $matches);
		return array_map('intval', $matches[0]);
	}

	function processTimes(string $times): array {
		return matchNumbers($times);
	}

	function processRecords(string $records): array {
		return matchNumbers($records);
	}

	return [
		'times'		=> processTimes(fgets($inputFile, 4096)),
		'records'	=> processRecords(fgets($inputFile, 4096))
	];
})();

// PART 1

function calculateChancesToWin(array &$data): void {
	$data['chancesToWin'] = [];
	foreach($data['times'] as $key => $limit) {
		$data['chancesToWin'][$limit] = 0;
		for($heldFor = 0; $heldFor <= $limit; $heldFor++) {
			$distance = $heldFor * ($limit - $heldFor);
			if($distance > $data['records'][$key]) {
				$data['chancesToWin'][$limit] += 1;
			}
		}

	}
}

calculateChancesToWin($data);

$productOfChances = array_reduce(
	$data['chancesToWin'],
	function($carry, $wins) {
		return $carry * $wins;
	},
	1
);

// Part 2 - It's just a single race!

$data['times'] = [
	intval(array_reduce(
		$data['times'],
		function($carry, $time) {
			return $carry . $time;
		},
		''
	))
];

$data['records'] = [
	intval(array_reduce(
		$data['records'],
		function($carry, $time) {
			return $carry . $time;
		},
		''
	))
];

/**
 * This is a brute force method. Using a quadratic equation is shorter
 * and much more performant.
 */
calculateChancesToWin($data);

$waysToWin = array_reduce(
	$data['chancesToWin'],
	function($carry, $winArray) {
		return $carry + $winArray;
	},
	0
);

echo 'Part 1: ', $productOfChances, ' margin of error.', PHP_EOL, PHP_EOL;

echo 'Part 2: ', $waysToWin, ' ways to win.', PHP_EOL, PHP_EOL;

echo 'Completed in ' . (microtime(true) - $start), ' seconds.', PHP_EOL;
