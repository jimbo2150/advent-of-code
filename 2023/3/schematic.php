<?php

$inputFile = fopen('./input.txt', 'r');

$schematic = [];
$symbolLocations = [];
$gearRatios = [];

function processLine(string $line, array &$schematic, array &$symbolLocations) {
	$schematic[] = str_split(trim($line));
	preg_match_all('/[^\d\.]/', trim($line), $matches, PREG_OFFSET_CAPTURE);
	if(!empty($matches)) {
		foreach($matches[0] as $match) {
			$symbolLocations[] = [$match[1], count($schematic)-1, $match[0]];
		}
	}
}

function acquireWholeNumber(array &$schematic, int $x, int $y): array {
	$number = '';
	$position = [];
 	for($lx = $x; $lx > -1; $lx--) {
		if(is_numeric($schematic[$y][$lx])) {
			$number = $schematic[$y][$lx] . $number;
		} else {
			break;
		}
	}
	$position[0] = $lx + 1;
	for($lx = $x + 1; $lx < count($schematic[$y]); $lx++) {
		if(is_numeric($schematic[$y][$lx])) {
			$number = $number . $schematic[$y][$lx];
		} else {

			break;
		}
	}
	$position[1] = $lx - 1;
	return [$number, $position];
}

function findPartNumbers(array &$schematic, array &$symbolLocations, array &$gearRatios): array {
	$nums = [];
	foreach($symbolLocations as $symbolLocation) {
		$symbolNums = [];
		$minX = $symbolLocation[0] - 1;
		$maxX = $symbolLocation[0] + 1;
		$minY = $symbolLocation[1] - 1;
		$maxY = $symbolLocation[1] + 1;
		foreach(range($minY, $maxY) as $y) {
			foreach(range($minX, $maxX) as $x) {
				$symbol = $schematic[$y][$x];
				if(is_numeric($symbol)) {
					list($num, $position) = acquireWholeNumber($schematic, $x, $y);
					$numKey = $y . implode('-',$position);
					if(!isset($nums[$numKey])) {
						$nums[$numKey] = $symbolNums[] = $num;
					}
				}
			}
		}
		if ($symbolLocation[2] == '*' && count($symbolNums) == 2) {
			$gearRatios[] = array_product($symbolNums);
		}
	}
	return $nums;
}

while(false !== ($line = fgets($inputFile, 4096))) {
	processLine($line, $schematic, $symbolLocations);
}

$partNums = findPartNumbers($schematic, $symbolLocations, $gearRatios);

echo PHP_EOL;
//echo "Part numbers: ", implode(', ', $partNums), PHP_EOL;
echo "Part number total: ", array_sum($partNums), PHP_EOL;
echo "Gear ratio: ", array_sum($gearRatios), PHP_EOL;
