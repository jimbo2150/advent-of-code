<?php

$inputFile = fopen('./input.txt', 'r');

function processLine(string $line) {
	$line = explode(':', $line);
	$line[1] = explode(';', $line[1]);
	$cubes = [];
	$roundKey = 0;
	/**
	 * Convert line to rounds separated by color
	 */
	foreach($line[1] as $round) {
		preg_match_all('/(?<count>\d+)\s+(?<color>\w+)/', $round, $counts);
		foreach($counts['count'] as $key => $count) {
			@$cubes[$roundKey][$counts['color'][$key]] += intval($count);
		}
		$roundKey++;
	}
	return [
		filter_var($line[0], FILTER_SANITIZE_NUMBER_INT),
		$cubes
	];
}

$limits = [
	'red' => 12,
	'green' => 13,
	'blue' => 14,
];

$possible_total = 0;
$cube_power = 0;

while(false !== ($line = fgets($inputFile, 4096))) {
	$possible = true;
	list($game, $cubes) = processLine($line);
	echo 'Game ', $game, ':';
	$gameMins = [];
	/**
	 * Loop through all game rounds and check if any cube colors exceed the
	 * limits. If so, the game is deemed impossible. (part 1)
	 * Also calculate minimums needed for each color (part 2)
	 */
	foreach($cubes as $roundNum => $round) {
		$roundPossible = true;
		echo PHP_EOL, '    Round ', $roundNum+1, ':';
		foreach($round as $color => $count) {
			$gameMins[$color] = max($gameMins[$color] ?? 0, $count);
			echo ' ', $count, ' ', $color, ',';
			if($count > $limits[$color]) {
				$roundPossible = $possible = false;
			}
		}
	}
	/**
	 * Output minimum number of cube colors needed for this game
	 */
	echo PHP_EOL, '    Minimums:';
	foreach($gameMins as $color => $count) {
		echo ' ', $count, ' ', $color, ','; 
	}
	/**
	 * Calculate cube power (color minimums multiplied) for round and total
	 *     (part 2)
	 * Calculate game ID total (part 1)
	 */
	$cube_power += ($round_power = array_product($gameMins));
	$possible_total += $possible ? $game : 0;
	echo PHP_EOL, '    Round cube power: ', $round_power;
	if(!$possible) {
		echo PHP_EOL, '    *GAME IMPOSSIBLE*';
	}
	echo PHP_EOL;
}

echo PHP_EOL, 'Possible game id sum: ', $possible_total, PHP_EOL;
echo 'Total cube power: ', $cube_power, PHP_EOL;