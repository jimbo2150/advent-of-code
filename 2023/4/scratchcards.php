<?php

$inputFile = fopen('./input.txt', 'r');

if(!$inputFile) {
    die('Unable to open input file');
}

$results = [];
$total = [
	'score' => 0,
	'cards' => 0
];

function generateCard(int $id, array &$cardPile): void {
	if(!isset($cardPile[$id])) {
		$cardPile[$id] = [
			'score' => 0,
			'duplicates' => 0
		];
	}
}

function processDuplicates(int $id, int $winningCount, array &$cardPile): void {
	if($winningCount > 0) {
		__processDuplicateCount($id, $winningCount, $cardPile);
		if($cardPile[$id]['duplicates'] > 0) {
			foreach(range(1, $cardPile[$id]['duplicates']) as $duplicateId) {
				__processDuplicateCount($id, $winningCount, $cardPile);
			}
		}
	}
}

function __processDuplicateCount(int $id, int $winningCount, array &$cardPile) {
	foreach(range($id + 1, $id + $winningCount) as $duplicateId) {
		generateCard($duplicateId, $cardPile);
		$cardPile[$duplicateId]['duplicates'] += 1;
	}
}

function outputCardStats(int $id, array &$cardPile, array $data): void {
	echo "Card: $id\n";
	echo "Matches: " . $data['matches'] . "\n";
	echo "Score: " . $cardPile[$id]['score'] . "\n";
	echo "Duplicates: " . $cardPile[$id]['duplicates'] . "\n";
	echo "\n\n";
}

function processTotals(int $id, array &$cardPile, array &$total): void {
	$total['score'] += $cardPile[$id]['score'];
	$total['cards'] += 1 + $cardPile[$id]['duplicates'];
}

function processLine(string $line): array|null {
	$split = preg_split('/[:\|]/', $line);
	if(count($split) != 3) {
		return null; // Empty line or erroneous data
	}
	list($card, $own, $picks) = [
		intval(filter_var($split[0], FILTER_SANITIZE_NUMBER_INT)),
		preg_split('/[^\d]/', $split[1], -1, PREG_SPLIT_NO_EMPTY),
		preg_split('/[^\d]/', $split[2], -1, PREG_SPLIT_NO_EMPTY)
	];
	return [ $card, $own, $picks, array_intersect($own, $picks) ];
}

function scoreCard(int $id, array $winners, array &$cardPile): void {
	$matches = count($winners);
	$cardPile[$id]['score'] += $matches > 0 ? pow(2, $matches - 1) : 0;
}

while(false !== ($line = fgets($inputFile, 4096))) {
	if(null === (list($card, $own, $picks, $winners) = processLine($line))) {
		continue; // Empty line or erroneous data
	}
	generateCard($card, $results);
	processDuplicates($card, count($winners), $results);
	scoreCard($card, $winners, $results);
	processTotals($card, $results, $total);
	//outputCardStats($card, $results, ['matches' => count($winners)]);
}

fclose($inputFile);

echo "Total cards: " . $total['cards'] . "\n";
echo "Total score: " . $total['score'] . "\n";