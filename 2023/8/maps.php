<?php

$start = microtime(true);

$inputFilename1 = './input.txt';
$inputFile1 = fopen($inputFilename1, 'r');

$inputFilename2 = './input.txt';
$inputFile2 = fopen($inputFilename2, 'r');

$lrMap = [ 'L' => 0, 'R' => 1 ];

$buildMap = function($inputFile) use ($lrMap): array {

	$map = [ 'paths' => [] ];

	$map['key'] = array_values(
		str_replace(array_keys($lrMap), $lrMap, str_split(trim(fgets($inputFile, 4096))))
	);

	while(false !== ($line = fgets($inputFile, 4096))) {
		$line = trim($line);
		if(empty($line)) continue; // blank line
		preg_match('/^(?<key>\w{3})\s*=\s*\((?<l>\w{3}),\s*(?<r>\w{3})\)$/', $line, $matches);
		$map['paths'][$matches['key']] = [ $matches['l'], $matches['r'] ];
	}

	fclose($inputFile);

	return [$map['paths'], $map['key']];

};

$traverseMap = function(array $paths, array $key, Closure $start, Closure $step): int {
	$steps = 0;
	reset($paths);
	reset($key);
	$data = $start($paths, $key);
	while(++$steps < PHP_INT_MAX) {
		if($step($paths, $key, $data, $steps)) break;
	}
	return $steps;
};

$startCb = function(string $startKey) {
	return fn($paths, $key) => ['currentKey' => $startKey, 'direction' => current($key)];
};
$stepCb = function(Closure $endCb) {
	return function(array $paths, array &$keys, array &$data, int $steps) use ($endCb) {
		if(!isset($paths[$data['currentKey']])) {
			throw new Exception('Invalid path: ' . $data['currentKey']);
		} else if(
			count(array_unique($paths[$data['currentKey']])) < 2 &&
			in_array(
				$paths[$data['currentKey']][array_key_first($paths[$data['currentKey']])],
				['XXX', 'ZZZ']
			)
		) {
			throw new Exception('At end of map (both directions are the same)');
		}
		$data['currentKey'] = $paths[$data['currentKey']][$data['direction']];
		if(false === ($data['direction'] = next($keys))) {
			reset($keys);
			$data['direction'] = current($keys);
		}
		return $endCb($data);
	};
};

$steps1 = $traverseMap(
	...array_merge(
		$buildMap($inputFile1),
		[ $startCb('AAA'), $stepCb(fn(array $data) => $data['currentKey'] == 'ZZZ') ]
	)
);

$map2 = $buildMap($inputFile2);

$startFrom = function($map): array {
	$startFrom = [];
	foreach(array_keys($map[0]) as $key) {
		if(preg_match('/A$/', $key)) {
			$startFrom[$key] = 0;
		}
	}
	return $startFrom;
};

$splitSteps = (function(array $startFrom, $map) use ($traverseMap, $startCb, $stepCb) {
	foreach($startFrom as $start => $count) {
		$startFrom[$start] = $traverseMap(
			...array_merge(
				$map,
				[
					$startCb($start),
					$stepCb(fn(array $data) => preg_match('/Z$/', $data['currentKey']))
				]
			)
		);
	}
	return $startFrom;
})($startFrom($map2), $map2);

$steps2 = (
	fn(array $stepArray): int => (int) array_reduce($stepArray, 'gmp_lcm', 1)
)($splitSteps);

echo 'Part 1: ', 'Steps: ', $steps1, PHP_EOL;
echo 'Part 2: ', 'Steps: ', $steps2, PHP_EOL;

echo 'Completed in ' . (microtime(true) - $start), ' seconds.', PHP_EOL;
