<?php

$start = microtime(true);

$inputFilename1 = './input.txt';
$inputFile1 = fopen($inputFilename1, 'r');

function partitionArray(array $arr): array {
	return array_slice(
		array_map(
			fn($value, $key) => [$value, $arr[$key + 1] ?? null],
			$arr,
			array_keys($arr)
		),
		0,
		-1
	);
};

$results = (function($inputFile) {

	$results = [];

	$zeroOutDiff = function(array $row, int $lineNum) use (&$zeroOutDiff, &$results) {
		$results[$lineNum][] = $row;
 		if(count(array_filter($row)) < 1) return;
		$newRow = array_map(
			fn($entries) => $entries[1] - $entries[0],
			partitionArray($row)
		);
		$zeroOutDiff($newRow, $lineNum);
	};

	$exRight = function(array &$rev, int $rowIdx) {
		$left = $rev[$rowIdx][array_key_last($rev[$rowIdx])] ?? 0;
		$prevRow = $rev[$rowIdx-1] ?? [];
		$below = $prevRow[array_key_last($prevRow)] ?? 0;
		$rev[$rowIdx][] = $left + $below;
	};

	$exLeft = function(array &$rev, int $rowIdx) {
		$right = $rev[$rowIdx][array_key_first($rev[$rowIdx])] ?? 0;
		$prevRow = $rev[$rowIdx-1] ?? [];
		$below = $prevRow[array_key_first($prevRow)] ?? 0;
		array_unshift($rev[$rowIdx], $right - $below);
	};

	$extrapolate = function(int $lineNum) use (&$results, $exRight, $exLeft) {
		$rev = array_reverse($results[$lineNum]);
		$copy = $rev;
		foreach($copy as $rowIdx => $row) {
			$exRight($rev, $rowIdx);
			$exLeft($rev, $rowIdx);
		}
		$results[$lineNum] = array_reverse($rev);
	};

	$lineNum = 0;
	while(false !== ($line = fgets($inputFile, 4096))) {
		$zeroOutDiff(
			array_map('intval', preg_split('/[\s]+/', $line, -1, PREG_SPLIT_NO_EMPTY)),
			$lineNum
		);
		$extrapolate($lineNum++);
	}

	fclose($inputFile);

	return $results;

})($inputFile1);

$sum1 = array_reduce(
	$results,
	fn($carry, $row) => $carry + $row[0][array_key_last($row[0])],
	0
);

$sum2 = array_reduce(
	$results,
	fn($carry, $row) => $carry + $row[0][array_key_first($row[0])],
	0
);

echo 'Part 1: ', $sum1, PHP_EOL;
echo 'Part 2: ', $sum2, PHP_EOL;

echo 'Completed in ' . (microtime(true) - $start), ' seconds.', PHP_EOL;
