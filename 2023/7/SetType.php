<?php

enum SetType:int {

	case FIVE_KIND = 6;
	case FOUR_KIND = 5;
	case FULL_HOUSE = 4;
	case THREE_KIND = 3;
	case TWO_PAIR = 2;
	case ONE_PAIR = 1;
	case HIGH_CARD = 0;

	public static function fromCounts(array $counts): self {
		arsort($counts);
		$firstKey = key($counts);
		next($counts);
		$secondKey = key($counts);
		return
			$counts[$firstKey] == 5 ?
				self::FIVE_KIND :
				($counts[$firstKey] == 4 ? self::FOUR_KIND :
					($counts[$firstKey] == 3 && $counts[$secondKey] == 2 ? self::FULL_HOUSE :
						(
							($counts[$firstKey] == self::THREE_KIND->value ? self::THREE_KIND :
								(
									$counts[$firstKey] == self::TWO_PAIR->value &&
										$counts[$secondKey] == self::TWO_PAIR->value ?
									self::TWO_PAIR :
									($counts[$firstKey] == 2 ?
										self::ONE_PAIR :
										(self::HIGH_CARD)
									)
								)
							)
						)
					)
				);
	}

}
