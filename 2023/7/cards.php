<?php

require './SetType.php';

$start = microtime(true);

$inputFilename = './input.txt';
$inputFile = fopen($inputFilename, 'r');

$parseData = function($jokersWild = false) use ($inputFile): array {

	$rounds = [];

	$replace = [
		'T' => 10,
		'J' => $jokersWild ? 1 : 11,
		'Q' => 12,
		'K' => 13,
		'A' => 14,
	];

	$parseHand = function(string $cards, int $bid) use ($replace, $jokersWild): array {

		$sets = array_fill_keys(array_merge(range(1, 9), array_keys($replace)), 0);
		$hand = str_split($cards);
		$handValue = '';

		foreach($hand as $card) {
			$handValue .= str_pad(
				str_replace(array_keys($replace), $replace, $card),
				2,
				'0',
				STR_PAD_LEFT
			);
			$sets[$card] += 1;
		}

		if($jokersWild) {
			$wildSets = array_diff_key($sets, [ 'J' => null ]);
			arsort($wildSets);
			$highSetCard = array_key_first($wildSets);
			$sets[$highSetCard] += $sets['J'] ?? 0;
			$sets['J'] = 0;
			$jokersWildCards = str_replace('J', $highSetCard, $cards);
		}

		$setType = SetType::fromCounts($sets);

		return [ $setType->value . $handValue, $bid, $hand, $setType, $jokersWildCards ?? '' ];
	};

	while(false !== ($line = fgets($inputFile, 4096))) {
		list($value, $bid, $hand, $setType, $jokersWildCards) = $parseHand(
			...preg_split('/\s+/', $line, -1, PREG_SPLIT_NO_EMPTY)
		);
		$rounds[$value] = $rounds[$value] ?? [];
		array_push(
			$rounds[$value],
			[
				'bid' => $bid,
				'cards' => $hand,
				'type' => $setType,
				'jokersWildCards' => $jokersWildCards,
			]
		);
	}

	ksort($rounds);

	fseek($inputFile, 0);

	return $rounds;

};

$calcScore = function(array $rounds): int {
	$rank = 1;
	$score = 0;
	foreach($rounds as $value => $hands) {
		foreach($hands as $hand) {
			$bid = $hand['bid'];
			echo 'Hand: ', implode('', $hand['cards']),
				!empty($hand['jokersWildCards']) ?
					' (' . $hand['jokersWildCards'] . ')' : '',
				' | Value ', $value,
				', Rank ', $rank, ' * Bid ', $bid, ' = ', $bid * $rank,
				' | Type: ', $hand['type']->name, PHP_EOL;
			$score += $bid * $rank++;
		}
	}
	return $score;
};

$rounds = $parseData();
$score1 = $calcScore($rounds);

$rounds = $parseData(true);
$score2 = $calcScore($rounds);

echo PHP_EOL;

echo 'Part 1: ', 'Total score is ', $score1, PHP_EOL, PHP_EOL;

echo 'Part 2: ', 'Total score is ', $score2, PHP_EOL, PHP_EOL;

echo 'Completed in ' . (microtime(true) - $start), ' seconds.', PHP_EOL;
