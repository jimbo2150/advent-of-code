<?php

$inputFile = fopen('./input.txt', 'r');
$total = 0;

$numWords = [
	'one' => 1,
	'two' => 2,
	'three' => 3,
	'four' => 4,
	'five' => 5,
	'six' => 6,
	'seven' => 7,
	'eight' => 8,
	'nine' => 9,
];

// Use group lookup of spelled words or digits inside lookahead to match
// overlapping words (for example, "eightwo" consisting of 8 and 2)
$pattern = '/(?=(' . implode('|', array_keys($numWords)) . '|\d))/';

while(false !== ($line = fgets($inputFile, 4096))) {
	// Match all spelled words or digits
	preg_match_all($pattern, $line, $matches);
	// Replace spelled words with numeric digits and convert all strings to integers
	$matches = array_map(
		'intval',
		str_replace(array_keys($numWords), array_values($numWords), $matches[1])
	);

	// Get line total and add to grand total consisting of the first and last
	// digits in the line.
	$total += $line_total = intval(
		$matches[array_key_first($matches)] . $matches[array_key_last($matches)]
	);

	// Output the unmodified line along with the numbers discovered, the
	// line total, and running grand total.
	echo trim($line), ' || ', (implode(',', $matches)), ' = ', $line_total,
		' (', $total, ')', PHP_EOL;
}

echo PHP_EOL, "Total: ", $total, PHP_EOL;
