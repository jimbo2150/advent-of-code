<?php

return (function(): Closure {

	function processSingleSeed(int $seedId) {
		
	}

	function processSeeds(string $line, array $data) {
		preg_match_all('/\d+/', $line, $matches);
		if(SEEDS_ARE_RANGES ?? false) {
			$ranges = array_chunk($matches[0], 2);
			foreach($ranges as $range) {
				for($i = $range[0]; $i <= $range[0] + $range[1]; $i++) {
					processSingleSeed($i);
				}
			}
			return;
		}
		foreach($matches[0] as $seedId) {
			processSingleSeed($seedId);
		}
	}

	return function(string $line, array $data): void {
		
		if($data['lineNum'] == 0) { // Seeds
			processSeeds($line, $data);
			return;
		}

	};

})();