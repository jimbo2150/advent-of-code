<?php

return function(PDO $db): void {
	$db->exec(
		'CREATE TABLE IF NOT EXISTS seeds(' .
			'id INTEGER PRIMARY KEY AUTOINCREMENT' .
		')'
	);
	$db->exec(
		'CREATE TABLE IF NOT EXISTS connections(' .
			'source VARCHAR(255), destination VARCHAR(255),' .
			'source_id INTEGER, destination_id INTEGER,' .
			'CONSTRAINT `connections_pk` ' .
				'PRIMARY KEY (source, source_id, destination, destination_id)' .
		')'
	);
};