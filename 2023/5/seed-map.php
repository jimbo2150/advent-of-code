<?php

$inputFilename = './input.txt';
$inputFile = fopen($inputFilename, 'r');

try {
	$db = new PDO('sqlite:./data-' . hash('sha256', $inputFilename) . '.db', null, null);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'SQLite error: ' . $e->getMessage();
}

define('SEEDS_ARE_RANGES', true);

if(!$inputFile) {
    die('Unable to open input file');
}

$start = microtime(true);

(function() use ($inputFile, $db) {
	$category = '';
	$lineNum = 0;
	$lineProcessor = require('./processors/line.php');
	$dbGenerator = require('./db/generate.php');
	$dbGenerator($db);
	while(false !== ($line = fgets($inputFile, 4096))) {
		$lineProcessor($line, ['db' => $db, 'lineNum' => $lineNum ]);
		$lineNum++;
	}
	fclose($inputFile);
})();

/*
echo 'Seed ', $min_key, ' has the lowest location value(s) of ',
	$min_value, PHP_EOL;
*/

echo 'Completed in ' . (microtime(true) - $start), ' seconds.', PHP_EOL;
